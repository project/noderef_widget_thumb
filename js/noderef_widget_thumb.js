// $Id:
/**
 * @file
 * JavaScript for the Node Reference widget thumbnail asynchronous change.
 */

// Add this to the jQuery namespace.
(function ($) {
  
  Drupal.noderefWidgetThumb = Drupal.noderefWidgetThumb || {};
  /**
   * Add on change to input from settings.
   */
  Drupal.behaviors.noderefWidgetThumb = function(context) {
    var self = Drupal.noderefWidgetThumb;
    //adminjaneliacustom-nodereference-autocomplete

    // Process an individual nodereference autocomplete widget.
    $('.noderefwidgetthumb-nodereference-autocomplete:not(.noderefwidgetthumb-processed)', context).addClass('noderefwidgetthumb-processed').each(function() {
      var $nodereference = $(this);
      var prev_nid = self.getNid($nodereference.val());
      $nodereference.bind('change', function() {
        var nid = self.getNid($nodereference.val());
        if (nid == '' || prev_nid != nid) {
          prev_nid = nid;
          var thumbId = '#' + $nodereference.attr('id') + '-noderef-widget-thumb';
          // This function will get exceuted after the ajax request is completed successfully
          var updateThumb = function(data) {
            // The data parameter is a JSON object. The “products” property is the list of products items that was returned from the server response to the ajax request.
            $(thumbId, context).html(data.image);
          }
          $.ajax({
            type: 'GET',
            url: '/noderefwidgetthumb/js/thumb/'+nid, // Which url should be handle the ajax request. This is the url defined in the <a> html tag
            success: updateThumb, // The js function that will be called upon success request
            dataType: 'json', //define the type of data that is going to get back from the server
          });
        }
      });
    });
  }
  
  /**
   * Extract the nid from the given nodereference value.
   */
  Drupal.noderefWidgetThumb.getNid = function(value) {
    var regExp = /^.*\[\s*nid\s*:\s*([0-9]+)\s*\]\s*$/;
    if (!regExp.test(value)) {
      return -1;
    }
    var nid = value.replace(regExp, '$1');
    return (nid.length > 0 ? parseInt(nid) : -1);
  };
  
})(jQuery);