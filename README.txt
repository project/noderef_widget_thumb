;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Nodereference Widget Thumbnail
;;// $Id$ 
;; Original author: kmadel (http://drupal.org/user/288818)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CONTENTS OF THIS FILE
=====================
- OVERVIEW
- REQUIREMENTS
- INSTALLATION


OVERVIEW
========

The Nodereferenc Widget Thumbnail module provides a configurable way to associate an 
image from a cck imagefield to be use as a thumbnail for nodereference autocomplete 
widget on the referencing node.


REQUIREMENTS
============

- CCK and Node Reference.
  http://drupal.org/project/cck
  
- FileField.
  http://drupal.org/project/filefield
  
- ImageField.
  http://drupal.org/project/imagefield

- Imagecache.
  http://drupal.org/project/imagecache
  
Highly Recommended 
- Node Relationships.
  http://drupal.org/project/noderelationships


INSTALLATION
============

- Be sure to install all dependent modules.

- Copy all contents of this package to your modules directory preserving
  subdirectory structure.

- Go to Administer -> Site building -> Modules to install module.

- The image based file field to use for a referenced content type (node type) is specified in the edit form of that content type. 
  As long as there is at least one File field with a widget type of Image specified on your content type, you will have 
  a new 'Nodereference Widget Thumbnail settings' with a drop down that allows you to select one field to be utilized 
  by the content types that reference this content type as a nodereference field.

- Review the settings of your nodereference fields to make sure related
  content types are explicitly specified in the option "Content types that
  can be referenced". And enable the 'Enable Thumbnail' checkbox.
  
- This module has also been thoroughly tested with the excellent noderelationships module which is highly recommended.

